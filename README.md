# Accelerated C++: Practical Programming by Example - Exercises
A collection of exercises I am completing to study C++ from the book: "Accelerated C++: Practical Programming by Example". Koenig, Andrew, et. al. Addison-Wesley Professional (2000).
